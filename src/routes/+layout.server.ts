import { env } from '$env/dynamic/private';

/** @type {import('./$types').LayoutServerLoad} */
export async function load() {
    if(env.SUPABASE_PATH != undefined){
        return {
            supabase_path: env.SUPABASE_PATH,
            supabase_key: env.SUPABASE_KEY
        }
    }else{
        return {
            supabase_path: 'https://ucwhyfpqwpjpaydfweyg.supabase.co',
            supabase_key: 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InVjd2h5ZnBxd3BqcGF5ZGZ3ZXlnIiwicm9sZSI6ImFub24iLCJpYXQiOjE2ODQ4OTM3MjUsImV4cCI6MjAwMDQ2OTcyNX0.VMVSws3h2DDoFNauqBOQKrrHKmV5HJdeqQireSBZxmI'
        }
    }
}
