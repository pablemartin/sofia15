import { createClient } from '@supabase/supabase-js';

export function convert (data) {
    let database_path = data.supabase_path;
    let database_key = data.supabase_key;
    return {
        supabase_path: database_path,
        supabase_key: database_key
    }
}

export async function encrypt(key, message){
		return await window.crypto.subtle.encrypt(
			  { name: "RSA-OAEP"},
			  key,
			  message
		);
}

export async function generate(){
		let keyPair = await window.crypto.subtle.generateKey(
			  {
				    name: "RSA-OAEP",
				    modulusLength: 4096,
				    publicExponent: new Uint8Array([1, 0, 1]),
				    hash: "SHA-256",
			  },
			  true,
			  ["encrypt", "decrypt"]
		);
		return keyPair;
}

export async function decrypt(key, content) {
		return await window.crypto.subtle.decrypt(
			  { name: "RSA-OAEP" },
			  key,
			  content
		);
}

export async function client (path, key) {
    return createClient(
        path,
        key
    )
}

export async function generateClient (db){
    let dec = new TextDecoder();
		let path = dec.decode(
			  await decrypt(
				    db.key.privateKey,
				    db.supabase_path
			  )
		);
		let key = dec.decode(
			  await decrypt(
				    db.key.privateKey,
				    db.supabase_key
			  )
		);
    return await client(path, key);
}

export async function setupDatabase (db) {
		let enc = new TextEncoder();
		let keyPair = await generate();
		let encrypted_path = await enc.encode(
        db.supabase_path
    );
		let supabase_path = await encrypt(
			  keyPair.publicKey,
			  encrypted_path
		);
		let encrypted_key = await enc.encode(
        db.supabase_key
    );
		let supabase_key = await encrypt(
			  keyPair.publicKey,
			  encrypted_key
		);
    return {
        key: keyPair,
        supabase_path: supabase_path,
        supabase_key: supabase_key
    }
}

export async function addinv (supabase_store, person) {
    const { data, error } = await supabase_store
        .from('invitados')
        .insert([
            person,
        ])
}

export async function getinv (supabase_store) {
    const { data } = await supabase_store
        .from("invitados")
        .select();
    return {
        invitations: data ?? [],
    };
}
