# Sofia 15

- Svelte
- Bootstrap
- Esri Arcgis
- Datebook

## Svelte


```bash
# create a new project in the current directory
npm create svelte@latest

# create a new project in my-app
npm create svelte@latest my-app

npm install

npm run dev

npm run dev -- --open

npm run build
```

You can preview the production build with `npm run preview`.
